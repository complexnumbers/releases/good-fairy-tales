# Build files for "Good Fairy Tales" album

Index file: https://complexnumbers.gitlab.io/releases/good-fairy-tales/

## License
### Good Fairy Tales (c) by Victor Argonov Project

Good Fairy Tales is licensed under a
Creative Commons Attribution-ShareAlike 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.
